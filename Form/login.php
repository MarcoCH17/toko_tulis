<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <title>Change Email</title>
    <style type="text/css">
        body {
            background-image: url('images.jpg');
        }

        .login-form-1 {
            padding: 5%;
            box-shadow: 0 5px 8px 0 rgba(0, 0, 0, 0.4), 0 9px 26px 0 rgba(0, 0, 0, 0.5);
        }

        .login-form-1 h3 {
            text-align: center;
            color: #333;
        }

        .login-container form {
            padding: 10%;
        }

        .input-group-text {
            background: transparent;
            border-radius: 0px;
            border: 0px;
            border-bottom: 0px;
            color: green;
            font-size: 18px;
        }

        .form-group {
            background: transparent;
            border-radius: 0px;
            border: 0px;
            border-bottom: 1px solid green;
            padding: 15px;
            font-size: 21px;
        }
    </style>
</head>

<body>
    <div class="container login-container">
        <div class="col-md-6 login-form-1 mx-auto my-5 text-center">
            <img src="logo.jpeg" width="100px" height="100px">
            <div class="gambar"></div>
            <form method="post" action="http://localhost/toko_tulis/tulis/index.php/admin/products">
                <div class="form-group">
                    <label for="exampleInputEmail1">Username</label>
                    <span class="input-group-text"><i class="fas fa-seedling"></i><input type="text" class="form-control" id="email1" placeholder="Enter email" name="email"></span>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Password</label><span class="input-group-text"><i class="fas fa-unlock-alt"></i><input type="password" class="form-control" id="password" placeholder="Enter password" name="password"></span>
                </div>
                <input type="button" class="btn btn-primary col-md-12" id="submit" value="login" onclick="validate()">
                <div class="form-group">
                    <input type="hidden" class="form-control">
                </div>
            </form>
        </div>

        <script language="javascript">
            var attempt = 3; // Variable to count number of attempts.
            // Below function Executes on click of login button.
            function validate() {
                var username = document.getElementById("email1").value;
                var password = document.getElementById("password").value;

                if (username == "admin" && password == "admin") {
                    alert("Login successfully");
                    window.location = "http://localhost/toko_tulis/tulis/index.php/admin/products"; // Redirecting to other page.
                    return false;
                } else {
                    attempt--; // Decrementing by one.
                    alert("You have left " + attempt + " attempt;");
                    // Disabling fields after 3 attempts.
                    if (attempt == 0) {
                        document.getElementById("username").disabled = true;
                        document.getElementById("password").disabled = true;
                        document.getElementById("submit").disabled = true;
                        return false;
                    }
                }
            }
        </script>

</html>